"use strict";
import React from 'react';
import ReactDOM from 'react-dom';
import './style.scss';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './app';
import allreducers from './reducers';

import 'semantic-ui-css/semantic.min.css';
import './style.scss';

const store = createStore(
	allreducers
);

class Client extends React.Component {
	render() {
		// console.info(store.getState());
		return (
			<Provider store={store}>
				<App />
			</Provider>
		)
	}
}

const tfunc = function(){
	return <p>hey</p>
}
const ttfunc = ()=>{
	return <p>hey</p>
}

ReactDOM.render(<Client />,document.querySelector('#root'));

// function Client() {
//     // return "hello"
//     // const e = React.createElement;

//     // Display a "Like" <button>
//     return <p className="myTest">Hello, raju</p>;
// }

// var a=function(){
//     let rn=document.querySelector('#root');
//     // console.info(rn.parentNode);
//     let c=document.createElement('div');
//     c.innerHTML="<p style=\"color:'red'\">raju</p>"; 
//     rn.appendChild(c);
//     // rn.textContent="rajasekhar";
// };
// a();
// // document.write("Hello");