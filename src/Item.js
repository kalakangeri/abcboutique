"use strict";
import React, { Fragment } from 'react';
import { Image, Card, Icon, Segment, Responsive, Header, Container, Grid, Label } from 'semantic-ui-react';

class Item extends React.Component {
	constructor(){
		super();
		

	}
	render() {
		// console.info(store.getState());
		// console.info(this.props.type);
		return (
			<Fragment>
				
				<Card style={{'paddingBottom':'0'}} onClick={this.props.selected}>
					<Image fluid src={this.props.imgSrc} />
					<Card.Content style={{'paddingBottom':'0'}}>
						<Card.Header textAlign='left' style={{'marginBottom':'-20','paddingBottom':'-20'}}>
								<Label ribbon='right' style={{'margin':'0'}}  >
									Rs.{this.props.price}/-
								</Label>
							<div style={{'margin':'0','padding':'0','top':'-20px','position':'relative'}}>
								{this.props.name}
							</div>
						</Card.Header>
						{/* <Card.Meta textAlign='left' style={{'marginTop':'-20','paddingTop':'-20'}}>
							<span className='date' style={{'paddingTop':'-20','paddingBottom':'-20'}}>Limited availability</span>
						</Card.Meta>
						<Card.Description textAlign='left'>Made of pure fiber.</Card.Description> */}
					</Card.Content>
				</Card>
			</Fragment>
		)
	}
}
export default Item
