"use strict";
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MenuBar from './menuBar';
import { nav } from './actions';
import Home from './home';
import About from './about';
import Contact from './contact';
import Collections from './collections';
import { Button, Grid, Image, Segment, Container } from 'semantic-ui-react';

class App extends React.Component {
	render() {
		// console.info(store.getState());
		return (
			<Fragment>
				<MenuBar />
				<Container className='collections' fluid>
				{
					this.props.contents.activeItem == "about" ? <About {...this.props} /> : this.props.contents.activeItem == "contact" ? <Contact {...this.props} /> : this.props.contents.activeItem == "womenswear" ? <Collections type='women' {...this.props} /> : this.props.contents.activeItem == "kidswear" ? <Collections type='kids' {...this.props} /> : <Home {...this.props} />
				}
				</Container>
				<Footer />
			</Fragment>
		)
	}
}

class Footer extends React.Component {
	render() {
		// console.info(store.getState());
		return (
			<Segment inverted color='pink' style={{'padding':'0px'}}>
				<Container>
					<Grid columns={3} divided inverted stackable color='pink'>
						<Grid.Column width={4}>
							<Grid.Row>
								<b>Address</b>
							</Grid.Row>
							<Grid.Row>
								ABC Boutique<br />
								
								Hyderabad <br />
								Telangana, India.
							</Grid.Row>
						</Grid.Column>

						<Grid.Column width={8}>
							<Grid.Row>
								<b>Email</b>
							</Grid.Row>
							<Grid.Row>
								info@abcboutique.com,
								abcboutique@gmail.com
							</Grid.Row>
							<Grid.Row>
								<b>Phone</b>
							</Grid.Row>
							<Grid.Row>
								+91 XXXXX XXXXX
							</Grid.Row>
						</Grid.Column>

						<Grid.Column width={4}>
							<Grid.Row>
								<b>Follow Us</b>
							</Grid.Row>
							<Grid.Row>
								<Button circular color='facebook' icon='facebook' />
							</Grid.Row>
							<Grid.Row>
								<Button circular color='twitter' icon='twitter' />
							</Grid.Row>
							<Grid.Row>
								<Button circular color='linkedin' icon='linkedin' />
							</Grid.Row>
							<Grid.Row>
								<Button circular color='google plus' icon='google plus' />
							</Grid.Row>
						</Grid.Column>

					</Grid>
				</Container>
			</Segment>
		)
	}
}

var mapStateToProps = (state) => {
	return {
		contents: state.contents
	};
}

var mapDispatchToProps = (dispatch) => {
	return bindActionCreators({ nav: nav }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
