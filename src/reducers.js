import { combineReducers } from 'redux';

const initialContentState = {
	activeItem: "home"
};

const contentReducers = function (
	state = initialContentState, action) {
	let tmpObj = "";
	switch (action.type) {
		case 'NAV':
			// console.info("userNAV");
			tmpObj = JSON.parse(JSON.stringify(state));
			tmpObj.activeItem = action.payload;
			return tmpObj;
		default:
			return state;
	}
}

// "https://source.unsplash.com/900x900/?design"
const initialItemState = { 
	allItems: [
		{
			id: "0",
			name: "Item",
			price: 999,
			imgSrc:"https://react.semantic-ui.com/images/avatar/large/matthew.png",
			createdDate:"27/11/2018",
			available:"Active"
		},
		{
			id: "1",
			name: "Design",
			price: 1050,
			imgSrc:"https://source.unsplash.com/900x900/?DESIGN",
			createdDate:"27/11/2018",
			available:"Active"
		},
		{
			id: "2",
			name: "Indian style",
			price: 1999,
			imgSrc:"https://source.unsplash.com/900x900/?indiantradition",
			createdDate:"27/11/2018",
			available:"Active"
		},
		{
			id: "3",
			name: "Fashion wear",
			price: 1299,
			imgSrc:"https://source.unsplash.com/900x900/?winterwear",
			createdDate:"27/11/2018",
			available:"Active"
		}
	],
	curItemView:0
};
const itemReducers = function (
	state = initialItemState, action) {
	// alert(action.type);
	let tmpObj = "";
	switch (action.type) {
		case 'ITEM_SELECTED':
			// console.info("BID_ITEM_SELECTED: "+action.payload);
			tmpObj=JSON.parse(JSON.stringify(state));
			tmpObj.curItemView=action.payload;
			return tmpObj;
		// case 'BID_REQUEST_ACTION':
		// 	// console.info("BID_ITEM_SELECTED: "+action.payload);
		// 	tmpObj=JSON.parse(JSON.stringify(state));
		// 	tmpObj.items.map((each,i)=>{
		// 		if(each.id==action.payload.bidId){
		// 			// console.info(each.vendors[action.payload.vendorKey].status);
		// 			each.vendors[action.payload.vendorKey].status = action.payload.response
		// 		}
		// 	})
		// 	return tmpObj;
		default:
			return state;
	}
}


const allreducers = combineReducers(
	{
		contents: contentReducers,
		items: itemReducers
	}
);

export default allreducers;