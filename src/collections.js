"use strict";
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Image, Segment, Responsive, Header, Container, Grid, Card } from 'semantic-ui-react';
import Item from './Item';
import { itemSelected } from './actions';

class Collections extends React.Component {
	render() {
		// console.info(store.getState());
		// console.info(this.props.type);
		return (
			<Fragment>
				{/* <ImageBoard /> */}
				<Pics {...this.props} />
			</Fragment>
		)
	}
}

class ImageBoard extends React.Component {
	constructor() {
		super();
	}
	render() {
		// console.info(store.getState());
		// console.info(Responsive.onlyComputer);
		// console.info(window.innerWidth);
		return (
			<Fragment>
				{/* <Image src='./src/assets/gallerymain.jpg' fluid ></Image> */}
				{/* <Segment.Group compact raised style={{'marginLeft':window.innerWidth >= Responsive.onlyComputer.minWidth && '20%'}}>
					<Segment>
						<Image src='./src/assets/2.jpeg'/>
					</Segment>
					<Segment >
						<Image src='./src/assets/3.jpeg' />
					</Segment>

					<Image src='./src/assets/4.jpeg' />
					<Image src='./src/assets/5.jpeg' />
					<Image src='./src/assets/6.jpeg' />
				</Segment.Group> */}
			</Fragment>
		)
	}
}


class Pics extends React.Component {
	constructor() {
		super();
		this.cardSelected=this.cardSelected.bind(this);
	}
	cardSelected(e,id){
		console.info("cardSelected");
		this.props.itemSelected(id);
	}
	render() {
		console.info(this.props.items.allItems);
		return (
			<Fragment>
				{/* <Segment> */}
				<br />
				<Container >
					<Header as='h2' textAlign='center' color="blue">
						<Header.Content>
							{this.props.type === 'women' ? <b>Women's wear</b> : <b>Kid's wear</b>}
						</Header.Content>
					</Header>
				</Container>
							<br />
				<Container textAlign='justified' fluid style={{'padding':'1%'}}>
					<Grid stackable>
						<Grid.Row>
							<Grid.Column width={8}>
							<Card.Group itemsPerRow='3' stackable>
								{this.props.items.allItems.map(item=>{
									return <Item key={item.id} name={item.name} price={item.price} imgSrc={item.imgSrc} selected={e=>this.cardSelected(e,item.id)}></Item>
								})}
								{/* 
								<Item name={item.name} price={item.price} imgSrc={item.imgSrc} selected={this.cardSelected}></Item>
								<Item name='Item' price='999' imgSrc='https://react.semantic-ui.com/images/avatar/large/matthew.png' selected={this.cardSelected}></Item>
								<Item name='Design' price='1050' imgSrc='https://source.unsplash.com/180x180/?DESIGN' selected={e=>this.cardSelected(e,1)}></Item>
								<Item name='Indian style' price='1999' imgSrc='https://source.unsplash.com/180x180/?indiantradition' selected={this.cardSelected}></Item>
								<Item name='Fashion wear' price='1299' imgSrc='https://source.unsplash.com/180x180/?winterwear' selected={this.cardSelected}></Item> */}
							</Card.Group>
								
							</Grid.Column>
							<Grid.Column width={8}>
								<Image src={this.props.items.allItems[this.props.items.curItemView].imgSrc} size='massive'/>
							</Grid.Column>
						</Grid.Row>
					</Grid>

				</Container>
				{/* </Segment> */}
			</Fragment>
		)
	}
}

var mapStateToProps = (state) => {
	return {
		items: state.items
	};
}

var mapDispatchToProps = (dispatch) => {
	return bindActionCreators({itemSelected:itemSelected}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Collections);

