"use strict";
import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Menu, Segment, Image, Button, Icon, Responsive } from 'semantic-ui-react';
import { nav } from './actions';


class MenuBar extends React.Component {
	constructor() {
		super();
		this.handleItemClick=this.handleItemClick.bind(this);
		this.downloadBrochure=this.downloadBrochure.bind(this);
	}
	handleItemClick(e, { name }) {
		// this.setState({ activeItem: name });
		this.props.nav(name);
	}
	downloadBrochure(){
		// './src/assets/2.jpeg'
		var link = document.createElement("a");
		link.download = 'brochure';
		link.href = './src/assets/2.jpeg';
		link.click();
	}
	render() {
		// const { activeItem } = this.props.contents.activeItem;
		// console.info(this.props.contents.activeItem);
		//   const { activeItem } = "Home";
		// console.log(window.innerWidth);
		// console.log("max"+Responsive.onlyMobile.maxWidth);
		// console.log(Responsive.onlyTablet.minWidth);

		return (
			<Segment inverted secondary color='pink' stacked style={{'padding':'0px','margin':'0px'}}>
				<Menu inverted  stackable color='pink' size='large' borderless pointing>
					<Menu.Item>
						<Image src='./src/assets/boutique.png' size='tiny' circular bordered />
					</Menu.Item>
					<Menu.Item header >
						<Menu.Header  content="ABC Boutique" style={{'fontFamily':'Pacifico,cursive','fontSize':'150%'}}/>
					</Menu.Item>
					
					<Menu.Menu position='right' style={{'fontVariant': 'small-caps'}} >
					{/* 'backgroundColor':'#f42d96' */}
					
					<Menu.Item
						name='home'
						onClick={this.handleItemClick}
						className='myMenuItem'
						style={{'color':this.props.contents.activeItem === 'home' && 'black'}}
						as='nav'
						content='Home'
						
					>
					</Menu.Item>

					<Menu.Item
						name='about'
						onClick={this.handleItemClick}
						className='myMenuItem'
						style={{'color':this.props.contents.activeItem === 'about' && 'black'}}
						as='nav'
						content='About'
					>
					</Menu.Item>

					<Menu.Item
						name='womenswear'
						onClick={this.handleItemClick}
						className='myMenuItem'
						style={{'color':this.props.contents.activeItem === 'womenswear' && 'black'}}
						as='nav'
						content={`Women's wear`}
					>
					</Menu.Item>

					<Menu.Item
						name='kidswear'
						onClick={this.handleItemClick}
						className='myMenuItem'
						style={{'color':this.props.contents.activeItem === 'kidswear' && 'black'}}
						as='nav'
						content={`Kid's Wear`}
					>
					</Menu.Item>

					<Menu.Item
						name='contact'
						onClick={this.handleItemClick}
						className='myMenuItem'
						style={{'color':this.props.contents.activeItem === 'contact' && 'black'}}
						as='nav'
						content='Contact Us'
					>
					</Menu.Item>
					</Menu.Menu>
					
				</Menu>
			</Segment>
		)
	}
}

var mapStateToProps = (state) => {
	return {
		contents: state.contents
	};
}

var mapDispatchToProps = (dispatch) => {
	return bindActionCreators({nav:nav}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuBar);

