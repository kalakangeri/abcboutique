"use strict";
import React, { Fragment } from 'react';
import { Icon, Card, Image, Header, Button, Container, Divider, Segment, Grid, Form } from 'semantic-ui-react';

class Contact extends React.Component {
	render() {
		// console.info(store.getState());
		return (
			<Fragment>
				<ImageBoard />
				<FormBoard />
				<Address />
			</Fragment>
		)
	}
}



class ImageBoard extends React.Component {
	constructor() {
		super();
	}
	render() {
		// console.info(store.getState());
		return (
			<Fragment>
				<Image src='https://source.unsplash.com/1285x200/?pattern' fluid ></Image>
			</Fragment>
		)
	}
}


class FormBoard extends React.Component {
	constructor() {
		super();

	}
	// handleChange(e, { value }) {
	// 	//this.setState({ value });

	// }

	render() {
		// console.info(store.getState());
		const options = [
			{ key: 'w', text: `Women's wear`, value: 'womenswear' },
			{ key: 'k', text: `Kid's wear`, value: 'kidswear' },
		];
		return (
			<Segment>
				<Container>
					<Form>
						<Form.Group widths='equal'>
							<Form.Input fluid label='Name' placeholder='Your name' />
							<Form.Input fluid label='Contact Number' placeholder='Your contact number' />
							<Form.Select fluid label='Item' options={options} placeholder='Item' />
						</Form.Group>
						{/* <Form.Group inline>
						<label>Size</label>
						<Form.Radio
							label='Small'
							value='sm'
							checked={value === 'sm'}
							onChange={this.handleChange}
						/>
						<Form.Radio
							label='Medium'
							value='md'
							checked={value === 'md'}
							onChange={this.handleChange}
						/>
						<Form.Radio
							label='Large'
							value='lg'
							checked={value === 'lg'}
							onChange={this.handleChange}
						/>
					</Form.Group> */}
						<Form.TextArea label='About' placeholder='Tell us more about you...' />
						<Form.Checkbox label='I agree to the Terms and Conditions' />
						<Form.Button>Submit</Form.Button>
					</Form>
				</Container>
			</Segment>
		)
	}
}


class Address extends React.Component {
	constructor() {
		super();

	}
	// handleChange(e, { value }) {
	// 	//this.setState({ value });

	// }

	render() {
		// console.info(store.getState());

		return (
			<Segment>
				<Container>
				<Grid stackable>
				<Grid.Row columns={3}>
					<Grid.Column width={5}>
						<Card>
							<Card.Content>
								<Card.Header>Address</Card.Header>
								<Card.Meta>
									<span className='address'>ABC Boutique</span>
								</Card.Meta>
								<Card.Description>
									<br />
									Hyderabad
									Telangana, India
								</Card.Description>
							</Card.Content>
						</Card>
					</Grid.Column>
					<Grid.Column width={5}>
						<Card>
							<Card.Content>
								<Card.Header>Email</Card.Header>
								<Card.Meta>
									
								</Card.Meta>
								<Card.Description>
									<span className='email'>info@abcboutique.com</span><br />
									<span className='email'>abcboutique@gmail.com</span>
									
								</Card.Description>
								{/* <Card.Description>#S4, TIE, Gate No -1, Balanagar, Hyderabad, Telangana, India - 500035.</Card.Description> */}
							</Card.Content>
						</Card>
					</Grid.Column>
					<Grid.Column width={5}>

						<Card>
							<Card.Content>
								<Card.Header>Phone</Card.Header>
								<Card.Meta>
									
								</Card.Meta>
								<Card.Description>
									<span className='phone'> +91 XXXXX XXXXX</span><br />
								</Card.Description>
								{/* <Card.Description>#S4, TIE, Gate No -1, Balanagar, Hyderabad, Telangana, India - 500035.</Card.Description> */}
							</Card.Content>
						</Card>
					</Grid.Column>
				</Grid.Row>
				</Grid>
				</Container>
			</Segment>
		)
	}
}

export default Contact
