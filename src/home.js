"use strict";
import React, { Fragment } from 'react';
import { Image, Header, Button, Container, Divider, Segment, Grid } from 'semantic-ui-react';


class Home extends React.Component {
	constructor() {
		super();
		this.routeToAbout = this.routeToAbout.bind(this);
	}
	routeToAbout(e) {
		// this.setState({ activeItem: name });
		this.props.nav('about');
	}
	render() {
		// console.info(store.getState());
		return (
			<Fragment>
				<ImageBoard />
				<WelcomeBoard />

			</Fragment>
		)
	}
}

class ImageBoard extends React.Component {
	constructor() {
		super();
	}
	render() {
		// console.info(store.getState());
		return (
			<Fragment>
				<Image src='https://source.unsplash.com/1285x600/?indianfashion' fluid />
			</Fragment>
		)
	}
}

class WelcomeBoard extends React.Component {
	constructor() {
		super();
	}
	render() {
		// console.info(store.getState());
		return (
			<Fragment>
				<Header as='h2' textAlign='center' color="blue">
					<Header.Content>
						<b>WELCOME TO ABC Boutique</b>
					</Header.Content>
				</Header>

				<Container textAlign='justified'>
					<p>
						Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
					</p>
					<p>
						It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
					</p>
					
					<br />
				</Container>
			</Fragment>
		)
	}
}


export default Home
